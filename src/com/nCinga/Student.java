package com.nCinga;

public class Student {
    private String name;
    private int rollNumber;
    private int batch;
    private String section;
    private Degree degree;
    public Student(String name,int rollNumber,int batch,String section, Degree degree) {
        this.rollNumber=0;
        this.batch=0;
        this.name=null;
        this.section=null;
        this.degree=null;

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    public void setDegree(Degree degree) {
        this.degree = degree;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setRollNumber(int rollNumber) {
        this.rollNumber = rollNumber;
    }

}
